#include "mem.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// Тест успешного выделения памяти и записи/чтения данных
void test_allocate() {
  void *heap_start = heap_init(4096);

  if (heap_start == NULL) {
    fprintf(stderr, "Test failed: Unable to initialize heap\n");
    return;
  }

  void *mem = _malloc(2048);
  if (mem == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 2048 bytes\n");
    heap_term();
    return;
  }

  printf("Test passed: Allocated 2048 bytes\n");

  heap_term();
}

// Тест освобождения одного блока из нескольких выделенных
void test_free_one() {
  void *heap = heap_init(1024);
  if (heap == NULL) {
    fprintf(stderr, "Test failed: Unable to initialize heap\n");
    return;
  }

  void *mem1 = _malloc(100);
  if (mem1 == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 100 bytes\n");
    heap_term();
    return;
  }

  printf("Test passed: Freeing one block of multiple allocated blocks\n");
  _free(mem1);

  heap_term();
}

// Тест освобождения двух блоков из нескольких выделенных
void test_free_two_of_multiple() {
  void *heap_start = heap_init(1024);

  if (heap_start == NULL) {
    fprintf(stderr, "Test failed: Unable to initialize heap\n");
    return;
  }

  void *mem1 = _malloc(100);
  void *mem2 = _malloc(200);
  void *mem3 = _malloc(50);

  _free(mem2);
  _free(mem1);
  _free(mem3);

  printf("Test passed: Freeing two blocks of multiple allocated blocks\n");

  heap_term();
}

// Тест расширения кучи за пределы текущего региона
void test_region_expand() {
  void *heap_start = heap_init(1024);

  if (heap_start == NULL) {
    fprintf(stderr, "Test failed: Unable to initialize heap\n");
    return;
  }

  // Выделение памяти
  void *mem1 = _malloc(800);
  void *mem2 = _malloc(500);
  if (mem2 == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 500 bytes\n");
    heap_term();
    return;
  }

  printf("Test passed: Region expansion beyond the current region\n");

  // Освобождение памяти
  _free(mem1);
  _free(mem2);

  // Завершение работы с кучей
  heap_term();
}

void test_heap_min_size() {
  void *heap = heap_init(512);
  if (heap == NULL) {
    fprintf(stderr, "Test failed: Unable to initialize heap\n");
    return;
  }
  uintptr_t mem1_adr, mem2_adr;
  int *mem1 = _malloc(20);
  if (mem1 == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 24 bytes\n");
    heap_term();
    return;
  }

  int *mem1_ = _malloc(20);
  if (mem1_ == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 24 bytes\n");
    _free(mem1);
    heap_term();
    return;
  }

  mem1_adr = (uintptr_t)mem1_;
  _free(mem1);
  _free(mem1_);

  int *mem2 = _malloc(24);
  if (mem2 == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 20 bytes\n");
    _free(mem1);
    heap_term();
    return;
  }

  int *mem2_ = _malloc(20);
  if (mem2_ == NULL) {
    fprintf(stderr, "Test failed: Unable to allocate memory 24 bytes\n");
    _free(mem2);
    heap_term();
    return;
  }

  mem2_adr = (uintptr_t)mem2_;
  _free(mem2);
  _free(mem2_);

  printf("%lx, %lx\n", mem1_adr, mem2_adr);
  assert(mem1_adr == mem2_adr);
  printf("Test passed: Heap min size\n");
  heap_term();
}

int main() {
  // Вызов тестов
  test_allocate();
  test_free_one();
  test_free_two_of_multiple();
  test_region_expand();
  test_heap_min_size();
  return 0;
}
